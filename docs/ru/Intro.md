# Documentation
Welcome to Vexera's documentation page!

Vexera is a **free Discord music bot** with lots of other features such as moderation for example. This documentation was made to assist users with using Vexera's features.  
Vexera is intended to be easy to use by our users.

If you have any questions, please do not hesitate to [join our community and support server by clicking here](https://discord.gg/f3qKB5Z).

## Topics:
1. [Getting Started](/docs/ru/gs)
2. [FAQ](/docs/ru/faq)
3. [Music](/docs/ru/music)
4. [Administration & Moderation](/docs/ru/admin)
5. [Permissions](/docs/ru/permissions)
6. [Autorole](/docs/ru/autorole)
7. [Settings & Customization](/docs/ru/settings)
8. [Fun, Memes & Images](/docs/ru/fun)
9. [Games](/docs/ru/games)
10. [Info](/docs/ru/info)