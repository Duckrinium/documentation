# Documentation
Welcome to Vexera's documentation page!

Vexera is a **free Discord music bot** with lots of other features such as moderation for example. This documentation was made to assist users with using Vexera's features.  
Vexera is intended to be easy to use by our users.

If you have any questions, please do not hesitate to [join our community and support server by clicking here](https://discord.gg/f3qKB5Z).

## Topics:
1. [Getting Started](/docs/en/gs)
2. [FAQ](/docs/en/faq)
3. [Music](/docs/en/music)
4. [Administration & Moderation](/docs/en/admin)
5. [Permissions](/docs/en/permissions)
6. [Autorole](/docs/en/autorole)
7. [Settings & Customization](/docs/en/settings)
8. [Fun, Memes & Images](/docs/en/fun)
9. [Games](/docs/en/games)
10. [Info](/docs/en/info)