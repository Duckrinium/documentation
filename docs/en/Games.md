# Games
Vexera doesn't have many features for games yet but they are being worked on constantly.

## Fortnite
You can check your Fortnite stats by using `+fortnite <plaform> <username>`.<br/>
A range of platforms can be found below:

Platform          | Argument
------------------|---------
PlayStation       | psn
Laptop / Computer | pc
Xbox              | xbox

For example `+fortnite psn iamlukej` would list `iamlukej`'s stats on PlayStation.

## osu!
Easily check how you're holding up on osu! with the following command: `+osu <option> <profile> [mode]`.<br/>
Valid options are:

Options |
--------|
best    |
recent  |
user    |

Valid modes are:

Modes    |
---------|
standard |
taiko    |
ctb      |
mania    |

A full example would be: `+osu best skiletro standard`. This command lists `skiletro`'s best beatmaps in standard mode.